import { Component } from '@angular/core';
import { Hero } from './hero';
import { HeroService } from './hero.service';
import { OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
    selector: 'heroes',
    templateUrl: './app/app.component.html',
    styleUrls: ['./app/app.component.css'],
    providers: [HeroService]
})
export class HeroesComponent implements OnInit{
    name = 'Tour of Heroes';
    hero: Hero;
    heroes:Hero[];

    constructor(
        private heroService: HeroService,
        private router: Router){}

    ngOnInit(): void {
        this.getHeroes();
    }

    getHeroes(): void {
        this.heroService.getHeroesSlowly().then(heroes => this.heroes = heroes);
    }


    onSelect(hero: Hero): void{
        this.hero = hero;
    }

    gotoDetail(): void {
        this.router.navigate(['/detail', this.hero.id]);
    }



}