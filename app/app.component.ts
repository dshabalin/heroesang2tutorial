import { Component } from '@angular/core'

@Component({
    selector: 'app',
    template: `
        <h1>{{title}}</h1>
        <nav>
            <nav>
                <a routerLink="/dashboard" routerLinkActive="active">Dashboard</a>
                <a routerLink="/heroes" routerLinkActive="active">Heroes</a>
            </nav>
        </nav>
        <router-outlet></router-outlet>
    `
})
export class AppComponent {
    title:string = 'Tour of Heroes';
}