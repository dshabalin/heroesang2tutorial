import { Component } from '@angular/core';
import { HeroService} from './hero.service';

@Component({
    moduleId: module.id,
    selector: 'dashboard',
    templateUrl: './dashboard.component.html',
    styleUrl: ['./dashboard.component.css'],
    providers: [HeroService]
})
export class DashboardComponent{
    heroes: Hero[];

    constructor(private heroService: HeroService){}

    ngOnInit(): void {
        this.getHeroes();
    }

    getHeroes(): void {
        this.heroService.getHeroesSlowly().then(heroes => this.heroes = heroes);
    }


}